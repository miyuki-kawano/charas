@extends('layout')

@section('content')
<h3>キャラ編集画面</h3>
<form action="/charas/{{$chara_contents->id}}" method="post" enctype="multipart/form-data">
    @method('PATCH')
    @csrf
    キャラの名前
    <br />
    <input type="text" name="name" value="{{$chara_contents->name}}">
    <br />
    登場作品
    <br />
    <input type="text" name="title" value="{{$chara_contents->title}}">
    <br />
    セリフ
    <input type="text" name="favorite_quote" value="{{$chara_contents->favorite_quote}}">
    <br />
    画像
    <br />
    <img src = "{{$image_data}}">
    <br />
    <input type="file" name="image" value="{{$chara_contents->image}}">
    <br />
    <input type="submit" value="更新">
    <input type="button" onClick='history.back();' value="戻る">
</form>

@endsection