@extends('layout')

@section('content')
<h3>キャラ追加</h3>
<form action="/charas" method="post" enctype="multipart/form-data">
    @csrf
    キャラの名前
    <br />
    <input type="text" name="name">
    <br />
    登場作品
    <br />
    <input type="text" name="title">
    <br />
    セリフ
    <input type="text" name="favorite_quote">
    <br />
    画像
    <br />
    <input type="file" name="image">
    <br />
    <input type="submit" value="登録">
    <input type="button" onClick='history.back();' value="戻る">
</form>


@endsection