@extends('layout')

@section('content')
<h3>キャラ詳細</h3>
<table border="1">
    <tr>
        <th>キャラNo</th>
        <th>キャラの名前</th>
        <th>登場作品</th>
        <th>台詞</th>
        <th>画像</th>
        <th colspan="2">変更</th>
    </tr>
    <tr>
        <td>{{$chara_contents->id}}</td>
        <td>{{$chara_contents->name}}</td>
        <td>{{$chara_contents->title}}</td>
        <td>{{$chara_contents->favorite_quote}}</td>
        <td><img src = "{{$image_data}}"></td>
        <td><a href="/charas/{{$chara_contents->id}}/edit">編集</a></td>
        <td>
        <form action="/charas/{{$chara_contents->id}}" method="POST">
            @method('DELETE')
            @csrf
            <input type="submit" value="削除"/>
        </form>
        </td>
    </tr>
</table>
<input type="button" onClick='history.back();' value="戻る">


@endsection