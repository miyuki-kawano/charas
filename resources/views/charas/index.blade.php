@extends('layout')

@section('content')
<h3>キャラ一覧</h3>
<a href="/charas/create">キャラ追加</a>
<table border="1">
    <tr>
        <th>キャラNo</th>
        <th>キャラの名前</th>
        <th>登場作品</th>
        <th>台詞</th>
        <th>画像</th>
        <th>詳細</th>
    </tr>
    @foreach($chara_contents as $value)
    <tr>
        <td>{{$value->id}}</td>
        <td>{{$value->name}}</td>
        <td>{{$value->title}}</td>
        <td>{{$value->favorite_quote}}</td>
        <td>
        @if($value->image == '')
        なし
        @else
        あり
        @endif</td>
        <td><a href="/charas/{{$value->id}}">詳細</a></td>
    </tr>
    @endforeach
</table>

@endsection