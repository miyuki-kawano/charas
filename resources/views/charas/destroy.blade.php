@extends('layout')

@section('content')
<h3>キャラ削除</h3>
<form action="/charas/{{$chara_contents->id}}" method="POST">
    @method('DELETE')
    @csrf
    <table border="1">
        <tr>
        <tr>
        <th>キャラNo</th>
        <th>キャラの名前</th>
        <th>登場作品</th>
        <th>台詞</th>
        <th>画像</th>
        </tr>
        <tr>
        <td>{{$chara_contents->id}}</td>
        <td>{{$chara_contents->name}}</td>
        <td>{{$chara_contents->title}}</td>
        <td>{{$chara_contents->favorite_quote}}</td>
        <td>{{$chara_contents->image}}</td>            </tr>
        </table>
            <input type="submit" value="削除">
            <input type="button" onClick='history.back();' value="戻る">
</form>
</table>

@endsection