<?php

use Illuminate\Database\Seeder;

class CharasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('charas')->insert([
            'name' => Str::random(10),
            'title' => Str::random(10),
            'favorite_quote' => Str::random(10),
            'image' => '',
        ]);
    }
}
