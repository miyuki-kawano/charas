<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCharasTableColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('charas', function (Blueprint $table) {
            //カラムのデータ型変更 MEDIUMBLOBにしたかったができないので実行せず
            //$table->データ型('image')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('charas', function (Blueprint $table) {
            //型をもとに戻す
            //$table->binary('image')->nullable()->change();
        });

    }
}
