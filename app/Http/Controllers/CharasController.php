<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chara;
use Illuminate\Support\Facades\DB;

class CharasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //一覧表示画面用
        $chara_contents = DB::table('charas')->get();
        return view('charas.index', ["chara_contents" => $chara_contents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //新規作成用
        return view('charas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //createで作成したものを保存する用
        $chara_contents = new Chara;
        $chara_contents->name = $request->name;
        $chara_contents->title = $request->title;
        $chara_contents->favorite_quote = $request->favorite_quote;

        if ($request->image == '') {
            $chara_contents->image = '';
        } else {
            //ファイル情報取得
            $chara_contents->image = file_get_contents($request->image);
        }

        //マイムタイプの設定
        $chara_contents->image_mime = 'image/jpeg';

        //データのアップロード
        $chara_contents->save();

        //一覧に戻る
        return redirect('charas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //保存したデータを個別に確認するページ用
        $chara_contents = DB::table('charas')->find($id);

        if ($chara_contents->image == '') {
            $image_data = '';
        } else {
            //バイナリデータの画像をbase64にエンコードする
            $image_data_base64 = base64_encode($chara_contents->image);
            $image_data = 'data:image/jpeg;base64,' . $image_data_base64;
        }
        return view('charas.show', ["chara_contents" => $chara_contents, "image_data" => $image_data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //保存した既存データを編集する
        $chara_contents = DB::table('charas')->find($id);

        if ($chara_contents->image == '') {
            $image_data = '';
        } else {
            //バイナリデータの画像をbase64にエンコードする
            $image_data_base64 = base64_encode($chara_contents->image);
            $image_data = 'data:image/jpeg;base64,' . $image_data_base64;
        }
        return view('charas.edit', ["chara_contents" => $chara_contents, "image_data" => $image_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //editで更新したものを保存する用
        $name = $request->name;
        $title = $request->title;
        $favorite_quote = $request->favorite_quote;

        if ($request->image == '') {
            $image_data = DB::table('charas')->where('id', $id)->value('image');
        } else {
            //ファイル情報取得
            $image_data = file_get_contents($request->image);
        }

        //マイムタイプの設定
        $image_mime = 'image/jpeg';

        DB::table('charas')
            ->where('id', $id)
            ->update([
                'name' => $name,
                'title' => $title,
                'favorite_quote' => $favorite_quote,
                'image' => $image_data,
                'image_mime' => $image_mime
            ]);
        //一覧に戻る
        return redirect('charas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('charas')
            ->where('id', $id)
            ->delete();
        //一覧に戻る
        return redirect('charas');
    }
}
