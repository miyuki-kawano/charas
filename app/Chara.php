<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chara extends Model
{
    //idはブラックリスト登録
    protected $guarded = ['id'];
    //ホワイトリスト登録
    protected $fillable = [
        'name',
        'title',
        'favorite_quote',
        'image',
        'image_mime'
    ];
}